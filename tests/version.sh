#!/bin/bash
#
# This is a simple version test to satisfy the RHEL8.1 onboard gating
# requirement.

# fail the whole test if any of the command pipelines below fails
set -ex

version=$(mcelog --version 2>&1 | awk ' { print $2 } ')
if [ "$version" == "unknown" ]; then
	echo "mcelog version is unknown."
	exit 1
fi

# if we get here, it's OK
